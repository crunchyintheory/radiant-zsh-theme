# vim:ft=zsh ts=2 sw=2 sts=2
# Some of these are copied directly from agnoster, some directly from bureau, but many are custom

CURRENT_BG='NONE'
LEFT=1
GIT_TOPLEVEL=''

() {
  local LC_ALL="" LC_CTYPE="en_US.UTF-8"

  #These segment characters are from Nerd Fonts

  SEGMENT_SEPARATOR=$'\ue0b8'
  SEGMENT_SUB_SEPARATOR=$'\ue0bf'

  R_SEGMENT_SEPARATOR=$'\ue0be'
  R_SEGMENT_SUB_SEPARATOR=$'\ue0bd'
}

prompt_segment() {
  local bg fg
  [[ -n $1 ]] && bg="%K{$1}" || bg="%k"
  [[ -n $2 ]] && fg="%F{$2}" || fg="%f"

  if [[ $CURRENT_BG != 'NONE' && $1 != $CURRENT_BG ]]; then
    if [[ $LEFT == 1 ]]; then
      echo -n " %{$bg%F{$CURRENT_BG}%}"
      echo -n "$SEGMENT_SEPARATOR"
    else
      echo -n " %{%K{$CURRENT_BG}%F{$1}%}"
      echo -n "$R_SEGMENT_SEPARATOR"
    fi
  fi
  echo -n "%{$bg%}%{$fg%} "
  CURRENT_BG=$1
  [[ -n $3 ]] && echo -n $3
}

prompt_end() {
  if [[ -n $CURRENT_BG ]]; then
    echo -n " %{%k%F{$CURRENT_BG}%}$SEGMENT_SEPARATOR"
  else
    echo -n "%{%k%}"
  fi
  echo -n "%{%f%}"
  CURRENT_BG=''
}

prompt_context() {
  prompt_segment 240 255 "%(!.%{%F{yellow}%}.)$USER"
  if [[ -n "$SSH_CLIENT" && ${SSH_CLIENT:0:9} != "127.0.0.1" ]]; then
    prompt_segment 240 255 "@%m"
  fi
}

prompt_git() {
  (( $+commands[git] )) || return
  local PL_BRANCH_CHAR
  () {
    local LC_ALL="" LC_CTYPE="en_US.UTF-8"
    PL_BRANCH_CHAR=$'\ue725'         # 
  }
  local ref dirty mode repo_path

  if $(git rev-parse --is-inside-work-tree >/dev/null 2>&1); then
    repo_path=$(git rev-parse --git-dir 2>/dev/null)
    dirty=$(parse_git_dirty)
    ref=$(git symbolic-ref HEAD 2> /dev/null) || ref="➦ $(git rev-parse --short HEAD 2> /dev/null)"
    if [[ -n $dirty ]]; then
      prompt_segment yellow black
    else
      prompt_segment green black
    fi

    if [[ -e "${repo_path}/BISECT_LOG" ]]; then
      mode=" <B>"
    elif [[ -e "${repo_path}/MERGE_HEAD" ]]; then
      mode=" >M<"
    elif [[ -e "${repo_path}/rebase" || -e "${repo_path}/rebase-apply" || -e "${repo_path}/rebase-merge" || -e "${repo_path}/../.dotest" ]]; then
      mode=" >R>"
    fi

    setopt promptsubst
    autoload -Uz vcs_info

    zstyle ':vcs_info:*' enable git
    zstyle ':vcs_info:*' get-revision true
    zstyle ':vcs_info:*' check-for-changes true
    zstyle ':vcs_info:*' stagedstr '\uf055'
    zstyle ':vcs_info:*' unstagedstr '\uf111'
    zstyle ':vcs_info:*' formats ' %u%c'
    zstyle ':vcs_info:*' actionformats ' %u%c'
    vcs_info
    echo -n "${ref/refs\/heads\//$PL_BRANCH_CHAR }${vcs_info_msg_0_%% }${mode}"
  fi
}

write_dir_text() {
    if echo $PWD | grep -q "^$HOME"; then
      local tmp=$(echo $PWD | sed -e "s/^${HOME//\//\\/}\///")
      prompt_segment 031 white '~'
      if [[ "$PWD" != "$HOME" ]]; then
        prompt_segment 237 white ${tmp//\// $SEGMENT_SUB_SEPARATOR }
      fi
    else
      prompt_segment 031 white '/'
      if [[ "$PWD" != "/" ]]; then
        prompt_segment 237 white ${${PWD:1}//\// $SEGMENT_SUB_SEPARATOR }
      fi
    fi
}

prompt_dir() {
  if [[ "$WORKSPACE" != "" ]]; then
    local tmp=$(echo $PWD | sed -e "s/^${WORKSPACE//\//\\/}\///")
    if [[ "$WORKSPACE_NAME" != "" ]]; then
      if [[ "${PWD:0:${#WORKSPACE}}" == "$WORKSPACE" ]]; then
        prompt_segment 214 black "$WORKSPACE_NAME"
        if [[ "$PWD" != "$WORKSPACE" ]]; then
          prompt_segment 237 white ${tmp//\// $SEGMENT_SUB_SEPARATOR }
        fi
      else
        prompt_segment 94 black "$WORKSPACE_NAME"
        write_dir_text
      fi
    fi
  else
    write_dir_text
  fi    
}

prompt_status() {
  local symbols
  symbols=()
  [[ $RETVAL -ne 0 && $? -ne 0 ]] && symbols+="%{%F{red}%}✘ $?"
  [[ $UID -eq 0 ]] && symbols+="%{%F{yellow}%}⚡"
  [[ $(jobs -l | wc -l) -gt 0 ]] && symbols+="%{%F{cyan}%}⚙"

  [[ -n "$symbols" ]] && prompt_segment black 255 "$symbols"
}

prompt_nvm() {
  local curv=$(nvm version)
  if [[ "$curv" != "system" ]]; then
    prompt_segment 77 black
    echo -n "\ue718 $curv"
  fi
}

prompt_docker_containers() {
  if [[ -n $WORKSPACE && -e $WORKSPACE/docker-compose.yml ]]; then
    if [[ $(($(docker-compose -f $WORKSPACE/docker-compose.yml ps | wc -l)-2)) -gt 0 ]]; then
      prompt_segment cyan black "\uf308 \uf0c1"
    else
      prompt_segment black cyan "\uf308 \uf0c1"
    fi
  elif [[ $DISABLE_DOCKER_PROMPT != 'true' ]]; then
    num=$(($(docker ps 2> /dev/null | wc -l)-1)) || 0
    if [[ $num -gt 0 ]]; then
      prompt_segment cyan black
      echo -n "\uf308 $num"
    fi
  fi
}

prompt_prompt() {
  prompt_segment black 031 '$'
}

prompt_gitlab() {
  [[ -n $GIT_TOPLEVEL && -e $GIT_TOPLEVEL/.gitlab-ci.yml ]] && prompt_segment white 172 '\uf296'
}

prompt_vscode() {
  [[ -n $GIT_TOPLEVEL && -e $GIT_TOPLEVEL/.vscode ]] && prompt_segment blue white '\ue70c'
}

left_prompt() {
  RETVAL=$?
  prompt_status
  prompt_context
  prompt_dir
  prompt_git
  prompt_end
}

right_prompt() {
  CURRENT_BG=''
  LEFT=0
  prompt_vscode
  prompt_gitlab
  prompt_nvm
  prompt_docker_containers
  echo -n " %{%f%k%}"
  CURRENT_BG=''
}

bottom_prompt() {
  CURRENT_BG='NONE'
  LEFT=1
  prompt_prompt
  echo -n " %{%f%k%}"
  CURRENT_BG=''
}

get_space () {
  local STR=$1$2
  local zero='%([BSUbfksu]|([FB]|){*})'
  local LENGTH=${#${(S%%)STR//$~zero/}}
  local SPACES=""
  (( LENGTH = ${COLUMNS} - $LENGTH - 1))

  for i in {0..$LENGTH}
    do
      SPACES="$SPACES "
    done

  echo $SPACES
}

build_prompt() {
  GIT_TOPLEVEL=$(git rev-parse --show-toplevel 2>/dev/null)
  LEFTP=$(left_prompt)
  RIGHTP=$(right_prompt)
  echo
  echo "$LEFTP$(get_space $LEFTP $RIGHTP)$RIGHTP"
  echo $(bottom_prompt)
}

PROMPT='%{%f%b%k%}$(build_prompt) '
